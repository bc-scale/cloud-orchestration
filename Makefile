init:
	@terraform init -backend=true
	@terraform validate
	@terraform plan -var-file=terraform.tfvars -out=tfplan

deploy:
	@terraform apply --auto-approve tfplan

destroy:
	@terraform destroy	--auto-approve