#============
# Project config
#============
variable "google-project" {
    description = "The ID of the google cloud project"
    type        = string
    default     = "forward-rain-302016"
}

variable "google-region" {
    description = "The name of region where google cloud resources will be created"
    type        = string
    default     = "europe-west2"
}

variable "google-zone" {
    description = "A specific zone in a given region"
    type        = string
    default     = "europe-west2-a"
}

#============
# Compute instance arguments
#============
variable "machine-type" {
    description = "The machine type to create"
    type        = string
    default     = "e2-standard-2"
}

variable "machine-image" {
    description = "The machine type to create"
    type        = string
    default     = "ubuntu-os-cloud/ubuntu-2004-lts"
}

variable "metadata" {
    description = "The Metadata key/value pairs to make available from within the instance"
    type        = map(string)
    default     = {}
}

variable "number-slave" {
    description = "The number of compute instances to create"
    type        = number
    default     = 3
}

#=================
# Compute Disk
#=================
variable "disk-size" {
    description = "Boot disk size in GB"
    type        = number 
    default     = 50
}

variable "disk-type" {
    description = "Boot disk type, can be either pd-ssd, local-ssd, or pd-standard"
    type        = string
    default     = "pd-standard"
}

variable "auto-delete" {
    description = "Whether or not the boot disk should be auto-deleted"
    type        = bool
    default     = true
}

variable "device-name" {
    description = "The name or self_link of the disk to attach to this instance"
    type        = string
    default     = ""
}

variable "disk-source" {
    description = "The name or self_link of the disk to attach to this instance"
    type        = string
    default     = null
}

variable "disk-mode" {
    description = "The mode in which to attach this disk"
    type        = string
    default     = "READ_WRITE"
}

variable "additional-disks" {
    description = "List of maps of additional disks"
    type = list(object({
        device_name  = string
        source       = string
        mode         = string
    }))
    default = []
}


#================
# Compute Network
#================
variable "network" {
    description = "The name or self_link of the network to attach this interface to"
    type        = string
    default     = null
}

variable "subnetwork" {
    description = "The name or self_link of the subnetwork to attach this interface to"
    type        = string
    default     = ""
}

variable "additional-networks" {
    description = "Additional network interface details for GCE, if any."
    type        = list(object({
        network    = string
        subnetwork = string
    }))
    default     = []
}
